#![allow(dead_code, unused_imports)]

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
// IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW OR AGREED TO IN WRITING WILL ANY COPYRIGHT HOLDER,
// OR ANY OTHER PARTY WHO MODIFIES AND/OR CONVEYS THE PROGRAM AS PERMITTED ABOVE, BE LIABLE TO YOU
// FOR DAMAGES, INCLUDING ANY GENERAL, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES ARISING OUT OF
// THE USE OR INABILITY TO USE THE PROGRAM (INCLUDING BUT NOT LIMITED TO LOSS OF DATA OR DATA BEING
// RENDERED INACCURATE OR LOSSES SUSTAINED BY YOU OR THIRD PARTIES OR A FAILURE OF THE PROGRAM TO
// OPERATE WITH ANY OTHER PROGRAMS), EVEN IF SUCH HOLDER OR OTHER PARTY HAS BEEN ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGES.
//
// If the disclaimer of warranty and limitation of liability provided above cannot be given local
// legal effect according to their terms, reviewing courts shall apply local law that most closely
// approximates an absolute waiver of all civil liability in connection with the Program, unless a
// warranty or assumption of liability accompanies a copy of the Program in return for a fee.
//
// All rights reserved.
//
// In jurisdictions that recognize copyright laws, the author or authors of this software dedicate
// any and all copyright interest in the software to the public domain. We make this dedication for
// the benefit of the public at large and to the detriment of our heirs and successors. We intend
// this dedication to be an overt act of relinquishment in perpetuity of all present and future
// rights to this software under copyright law.

use std::fs::{File, OpenOptions};
use std::io::{self, BufRead, Read, Seek, Write};
use std::net::{TcpListener, TcpStream};
use std::thread;

use anyhow::Result;

const WIDTH: u32 = 1280;
const HEIGHT: u32 = 720;

struct Client {
    stream: TcpStream,
    framebuffer: File,
}

fn fb_offset(x: u32, y: u32) -> u32 {
    (x + (WIDTH * y)) * 4
}

impl Client {
    fn fb_seek(&mut self, x: u32, y: u32) -> Result<()> {
        let offset = fb_offset(x, y);
        self.framebuffer
            .seek(std::io::SeekFrom::Start(offset.into()))?;
        Ok(())
    }

    fn fb_read(&mut self, x: u32, y: u32) -> Result<Rgb> {
        self.fb_seek(x, y)?;

        let mut buffer = [0u8; 4];
        self.framebuffer.read_exact(&mut buffer)?;
        let [b, g, r, _] = buffer;

        Ok(Rgb(r, g, b))
    }

    fn fb_write(&mut self, x: u32, y: u32, color: Rgb) -> Result<()> {
        self.fb_seek(x, y)?;

        let Rgb(r, g, b) = color;
        self.framebuffer.write_all(&[b, g, r, 255])?;

        Ok(())
    }

    fn handle_command(&mut self, words: Vec<&str>) -> Result<()> {
        let command = words.first().ok_or(anyhow::anyhow!("no command given"))?;

        match *command {
            "HELP" => {
                self.stream.write_all(b"welcome to slice pixelfuck")?;
            }
            "SIZE" => {
                let response = format!("SIZE {} {}\n", WIDTH, HEIGHT);
                self.stream.write_all(response.as_bytes())?;
                self.stream.flush()?;
            }
            "PX" => {
                if words.len() < 3 {
                    anyhow::bail!("invalid arguments to PX");
                }

                let x: u32 = words[1].parse()?;
                let y: u32 = words[2].parse()?;

                if words.len() == 4 {
                    let color: Rgb = words[3].parse()?;
                    self.fb_write(x, y, color)?;
                } else if words.len() == 3 {
                    let Rgb(r, g, b) = self.fb_read(x, y)?;
                    let response = format!("PX {} {} {:02X}{:02X}{:02X}", x, y, r, g, b);
                    self.stream.write_all(response.as_bytes())?;
                    self.stream.flush()?;
                }
            }
            _ => {}
        }

        Ok(())
    }

    fn handle(&mut self) {
        let cloned_stream = self.stream.try_clone().expect("failed to clone stream");
        let mut buffered_reader = io::BufReader::new(cloned_stream);

        loop {
            let mut line = String::new();
            match buffered_reader.read_line(&mut line) {
                Ok(0) => break,
                Ok(_) => {}
                Err(err) => eprintln!("error reading: {:?}", err),
            }

            let words = line.trim_end().split_ascii_whitespace().collect::<Vec<_>>();

            if let Err(err) = self.handle_command(words) {
                eprintln!("error: {:?}", err);
            }
        }
    }
}

#[derive(Copy, Clone)]
struct Rgb(u8, u8, u8);

impl std::str::FromStr for Rgb {
    type Err = anyhow::Error;

    fn from_str(s: &str) -> Result<Rgb> {
        let color: u32 = u32::from_str_radix(s, 16)?;
        let [_, r, g, b] = color.to_be_bytes();
        Ok(Rgb(r, g, b))
    }
}

fn handle_stream(stream: TcpStream) {
    let framebuffer = OpenOptions::new()
        .read(true)
        .write(true)
        .open("/dev/fb0")
        .expect("failed to open /dev/fb0");

    // TODO: limit the numbers of threads spawned lol
    thread::spawn(move || {
        let mut client = Client {
            stream,
            framebuffer,
        };

        client.handle();
    });
}

fn main() -> io::Result<()> {
    let listener = TcpListener::bind("0.0.0.0:33333");

    println!("listening on :33333");

    for stream in listener?.incoming() {
        match stream {
            Ok(stream) => handle_stream(stream),
            Err(err) => {
                eprintln!("failed to handle stream: {}", err)
            }
        }
    }

    Ok(())
}
